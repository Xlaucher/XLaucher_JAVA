/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xlaucher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ytong
 */
public class Laucher {

    private static String libs;

    public static void Lauch(LaucherInfo l) throws IOException, JSONException, StringIndexOutOfBoundsException {
        JSONObject JSON = new JSONObject(Use.readfile(Use.rundir() + "/.minecraft/versions/" + l.version + "/" + l.version + ".json"));
        JSONArray Array = JSON.getJSONArray("libraries");
        ArrayList libraries = new ArrayList();
        for (int i = 0; i < Array.length(); i++) {
            libraries.add(Array.getJSONObject(i).getString("name"));
        }

        for (int a = 0; a < libraries.size(); a++) {
            libraries.set(a, libraries.get(a).toString().replaceAll(":", "/"));
            // libraries[a]=  libraries[a].replaceAll("\\.", "/");
            String temp = libraries.get(a).toString();
            String temp4;
            String temp2[] = temp.split("/");
            String temp3 = temp2[0];
            temp2[0] = temp2[0].replaceAll("\\.", "/");
            temp = temp.replaceAll(temp3, temp2[0]);

            temp4 = "/" + temp2[temp2.length - 2] + "-" + temp2[temp2.length - 1] + ".jar";
            //    StringBuilder b = new StringBuilder(temp);
            if (temp.startsWith("/") == false) {
                temp = "/" + temp;
            }
            libraries.set(a, temp + temp4);

            if (new File(Use.rundir() + "/.minecraft/libraries" + libraries.get(a)).exists() == false) {
                libraries.remove(a);
//                System.out.println("文件" + Use.rundir() + "/.minecraft/libraries" + libraries.get(a) + "不存在");
            }

        }
        libs = "";
        for (int x = 0; x < libraries.size(); x++) {
            libs = libs + Use.rundir() + "/.minecraft/libraries" + libraries.get(x) + ";";
        }
        libs = libs.substring(0, libs.length() - 2);
        String CMD = GenLaucherCMD(libs, JSON.getString("mainClass"), JSON.getString("minecraftArguments"), l);
        try {
            if (CheckOS() == 0) {

                CMD = CMD.replace("/", "\\");
                System.out.println(CMD);
            }
            //   Runtime.getRuntime().exec(CMD);

            new File(Use.rundir() + "/x.bat").delete();
            Use.WriteFile(new File(Use.rundir() + "/x.bat"), CMD);
            Runtime.getRuntime().exec(CMD);

        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }

    static String GenLaucherCMD(String lib, String mainClass, String args, LaucherInfo l) throws IOException {
        String in = "";
        String user = args;
        user = user.replace("$", "");
        user = user.replace("{", "");
        user = user.replace("}", "");
        user = user.replaceAll("auth_player_name", l.username);
        user = user.replaceAll("version_name", l.version);
        user = user.replaceAll("game_directory", Use.rundir() + "/.minecraft");
        user = user.replaceAll("game_assets", Use.rundir() + "/.minecraft/assets");
        //user = user.replaceAll("auth_session", "{auth_session");
        //user = user.replaceAll("{auth_session", "${auth_session");
        //   user = user.replaceAll("${auth_session", "${auth_session}");
        in = l.java + " -Xmx" + l.Memory + " -Djava.library.path=" + Use.rundir() + "/.minecraft/natives -cp " + lib + ";" + Use.rundir() + "/.minecraft/versions/" + l.version + "/" + l.version + ".jar"
                + " " + mainClass + " " + user;
        in=in.replaceAll(":", ":/");
        return in;
    }

    public static int CheckOS() {
        String s = System.getProperty("os.name");
        //   System.out.println(s);
        if (s.contains("Windows")) {
            return 0;
        }
        if (s.contains("Linux")) {
            return 1;
        }
        if (s.contains("Mac")) {
            return 2;
        }
        return -1;
    }

}
